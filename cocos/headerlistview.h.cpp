#ifndef TIMEOFSILENS_HEADERLISTVIEW_H
#define TIMEOFSILENS_HEADERLISTVIEW_H

#include "cocos2d.h"
#include "UI/CocosGUI.h"


class HeaderListView : public cocos2d::ui::ListView
{
public:
	static HeaderListView* create(cocos2d::Node* header, float minHeaderHeight);
	virtual bool init(cocos2d::Node* header, float minHeaderHeight);
	virtual ~HeaderListView();

	virtual void update(float dt) override;

	virtual void insertCustomItem(cocos2d::ui::Widget* item, ssize_t index) override;
	virtual void removeAllItems() override;
	virtual cocos2d::ui::Widget* getItem(ssize_t index) const override;
	virtual ssize_t getIndex(cocos2d::ui::Widget* item) const override;
	virtual cocos2d::Vector<Node*>& getChildren() override;
	virtual const cocos2d::Vector<Node*>& getChildren() const override;

	void setHeaderRelativeOffsetCallback(std::function<void(float)> callback);

private:
	virtual cocos2d::Vector<cocos2d::ui::Widget*>& getItems() override;
	virtual void removeAllChildren() override;

private:
	cocos2d::Node*  _header             = nullptr;
	float           _minHeaderHeight    = 0;

	cocos2d::ClippingNode*  _headerClipper  = nullptr;
	cocos2d::DrawNode*      _headerStencil  = nullptr;

	std::function<void(float)> _headerOffsetChanged = nullptr;
};


#endif //TIMEOFSILENS_HEADERLISTVIEW_H