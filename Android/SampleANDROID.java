/*
 * Copyright (c) 2017. VironIT
 * Created by developer <developer@vironit.com>
 */

package com.vironit.livia.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.vironit.livia.App;
import com.vironit.livia.R;
import com.vironit.livia.presenter.abstracts.BasePresenter;
import com.vironit.livia.presenter.FaqPresenter;
import com.vironit.livia.presenter.model.Question;
import com.vironit.livia.utils.AppLog;
import com.vironit.livia.view.adapter.FaqAdapter;
import com.vironit.livia.view.fragment.abstracts.BaseFragment;
import com.vironit.livia.view.interfaces.IFaqView;
import com.vironit.livia.view.widgets.helpers.DividerItemDecoration;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * View which shows faq.
 */
public class FaqFragment extends BaseFragment implements IFaqView {

    /**
     * View for list of question and answers.
     */
    @BindView(R.id.rv_items) protected RecyclerView mRecyclerView;

    /**
     * View for refresh a list of question and answers.
     */
    @Nullable @BindView(R.id.srl_items) protected SwipeRefreshLayout mSwipeRefreshLayout;

    /**
     * Presenter for business logic.
     */
    @Inject FaqPresenter mPresenter;

    /**
     * Adapters provide a binding from a data set to {@link #mRecyclerView}.
     */
    private FaqAdapter mFaqAdapter;

    /**
     * Creates an instance.
     *
     * @param fragmentManager Your parent {@link android.support.v4.app.FragmentManager}.
     */
    public static BaseFragment newInstance(FragmentManager fragmentManager) {
        BaseFragment fragment = (BaseFragment) fragmentManager.findFragmentByTag(FaqFragment.class.getSimpleName());
        if (fragment == null) {
            fragment = new FaqFragment();
        }
        return fragment;
    }

    /**
     * @return {String} FaqFragment.class.getSimpleName()
     */
    @Override
    public String getFragmentTag() {
        return FaqFragment.class.getSimpleName();
    }

    /**
     * @return {int} R.string.fragment_faq_title
     */
    @Override
    public int getTitle() {
        return R.string.fragment_faq_title;
    }

    /**
     * @param savedInstanceState 
     */
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent().inject(this);
    }

    /**
     * @param {View} view 
     */
    @Override
    protected void onCreateView(View view) {
        super.onCreateView(view);
        ButterKnife.bind(this, view);
        initRecycler();
        initRefresh();
    }

    /**
     * @param {View} view
     * @param savedInstanceState 
     */
    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setWindowBackgroundColor(R.color.color_background);
        restoreActionBarColor();
        showActionBar();
        mPresenter.onCreate(this);
    }

    /**
     * @return {int} R.layout.fragment_faq
     */
    @Override 
    public int getLayoutResId() {
        return R.layout.fragment_faq;
    }

    /**
     * @return mPresenter
     */
    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    /**
     * @param questions
     */
    @Override
    public void showQuestions(final List<Question> questions) {
        mFaqAdapter.addAll(questions);
    }

    @Override
    public void clearData() {
        super.clearData();
        mFaqAdapter.clear();
    }

    @Override
    public void showProgressDialog() {
        AppLog.i(this, "showProgressDialog()");
        showRefreshing();
    }

    @Override
    public void dismissProgressDialog() {
        AppLog.i(this, "dismissProgressDialog()");
        dismissRefreshing();
    }

    /**
     * @return mSwipeRefreshLayout
     */
    @Override
    @Nullable
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return mSwipeRefreshLayout;
    }

    /**
     * recycler layout initialization
     */
    protected void initRecycler() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getContext(), R.drawable.divider_vertical_light)));
        mRecyclerView.setHasFixedSize(true);
        addOnScrollListener(mRecyclerView);
        mFaqAdapter = new FaqAdapter();
        mRecyclerView.setAdapter(mFaqAdapter);
    }

    /**
     * refresh layout initialization
     */
    private void initRefresh() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeResources(R.color.color_accent);
            mSwipeRefreshLayout.setOnRefreshListener(() -> getPresenter().repeatRequest());
        }
    }
}
